#lang racket

(require crypto)

(provide sha256d)

;; Avoid length extension attacks (?)
;; This is what tahoe-lafs does IIUC
(define (sha256d input)
  (digest 'sha256 (digest 'sha256 input)))
